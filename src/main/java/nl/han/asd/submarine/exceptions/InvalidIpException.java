package nl.han.asd.submarine.exceptions;

public class InvalidIpException extends RuntimeException {
    public InvalidIpException(String s) {
        super(s);
    }
}
